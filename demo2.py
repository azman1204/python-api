# demonstration query data using flask_sqlalchemy

from app import *
from models.user import User
from models.recipe import Recipe

app = create_app()
app.app_context().push()

# select * from user where username = '...'
user = User.query.filter_by(username='jack').first()
print(f'username={user.username}, email={user.email}')
print('*' * 100)

# select * from user
users = User.query.all()
for u in users:
    print(f'username={u.username}')

# select * from user where id = 1
# find by pk
auser = User.query.get(4)
print('*' * 100)
print(f'username={auser.username}')