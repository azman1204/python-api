# update and delete data

from app import *
from models.user import User
from models.recipe import Recipe

app = create_app()
app.app_context().push()

# update user set email='...' where id=1

user = User.query.get(1)
user.email = 'abu@yahoo.com'
db.session.commit()

# delete from user where id=1
me = User.query.get(1)
db.session.delete(me)
db.session.commit()

