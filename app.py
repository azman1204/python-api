from flask import Flask, render_template, jsonify, request
from flask_restful import Api
from resources.demo import DemoResource
from resources.mail import MailResource, MailHtmlResource, MailRecipeResource, MailAttachmentResource
from resources.recipe import RecipeListResource, RecipeResource, RecipePublishResource
from config import Config
from extensions import db, jwt, cur, conn
from flask_migrate import Migrate
from models.user import User
from models.recipe import Recipe
from resources.token import TokenResource, RefreshResource, RevokeResource, black_list
from resources.upload import UploadResource
from resources.user import UserListResource, UserResource, MeResource
from flask_cors import CORS
from flask_mail import Mail, Message


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    CORS(app, resources={r"/*": {"origins": "*"}})
    register_extensions(app)
    register_resources(app)
    Mail(app)  # setting the mail
    return app


def register_extensions(app):
    db.init_app(app)
    migrate = Migrate(app, db)
    jwt.init_app(app)

    @jwt.token_in_blocklist_loader
    # def check_if_token_in_blacklist(decrypted_token):
    def check_if_token_in_revoked(jwt_header, jwt_payload):
        print('checking jwt in black list..')
        # jti = decrypted_token['jti']
        jti = jwt_payload['jti']
        return jti in black_list


def register_resources(app):
    api = Api(app)
    api.add_resource(RecipeListResource, '/recipes')
    api.add_resource(RecipeResource, '/recipes/<int:recipe_id>')
    api.add_resource(RecipePublishResource, '/recipes/<int:recipe_id>/publish')
    api.add_resource(UserListResource, '/users')
    api.add_resource(UserResource, '/user/<string:username>')
    api.add_resource(TokenResource, '/token')
    api.add_resource(MeResource, '/me')
    api.add_resource(RefreshResource, '/refresh')
    api.add_resource(RevokeResource, '/revoke')  # logout
    api.add_resource(MailResource, '/mail2')  # plain text message
    api.add_resource(MailHtmlResource, '/mail3')  # html message
    api.add_resource(MailRecipeResource, '/mail-recipe')  # html message
    api.add_resource(MailAttachmentResource, '/mail-attachment')  # html message
    api.add_resource(UploadResource, '/upload')
    api.add_resource(DemoResource, '/demo')

    @app.route('/home')
    def home():
        return render_template('home.html')

    @app.before_request
    def logme():
        print("coming request...")
        ip = request.remote_addr
        data = request.path
        sql = f"INSERT INTO log (ip, user_id, data) VALUES ('{ip}', 1, '{data}')"
        cur.execute(sql)
        conn.commit()
        return {}, 200


if __name__ == '__main__':
    app = create_app()
    app.run(port=5000, debug=True)
