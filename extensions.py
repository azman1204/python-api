from http import HTTPStatus

from flask import request
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
import psycopg2
from psycopg2.extras import RealDictCursor

# these objects are instantiate only once
db = SQLAlchemy()
jwt = JWTManager()

# conn = psycopg2.connect("dbname=smilecook user=azman password=azman1204")
conn = psycopg2.connect(host='localhost', database='smilecook',
                        user='azman', password='azman1204', cursor_factory=RealDictCursor)
try:
    cur = conn.cursor()
    print("connection success")
except Exception as err:
    print("Connection failed", err)


def validate_token():
    # validate token coming from user
    token = request.headers.get('token')
    # get ip address
    ip = request.remote_addr
    sql = f"SELECT * FROM client WHERE token = '{token}' AND ip = '{ip}'"
    cur.execute(sql)
    rows = cur.fetchone()
    print(rows)

    if rows is None:
        return False
    else:
        return True
