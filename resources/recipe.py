from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from http import HTTPStatus
from models.recipe import Recipe
from models.user import User


class RecipeListResource(Resource):

    # return only published recipes
    # GET localhost:5000/recipes
    def get(self):
        recipe = Recipe()
        recipes = recipe.get_all_published()
        lst = []
        for recipe in recipes:
            lst.append({
                'id': recipe.id,
                'name': recipe.name,
                'description': recipe.description,
                'cook_time': recipe.cook_time
            })
        return lst, HTTPStatus.OK

    # POST localhost:5000/recipes
    @jwt_required()
    def post(self):
        json_data = request.get_json()
        name = json_data.get('name')
        description = json_data.get('description')
        num_of_servings = json_data.get('num_of_servings')
        cook_time = json_data.get('cook_time')
        directions = json_data.get('directions')

        # read the user info from jwt identity
        user = User.query.get(get_jwt_identity())

        recipe = Recipe(
            name=name,
            description=description,
            num_of_servings=num_of_servings,
            cook_time=cook_time,
            directions=directions,
            user_id=user.id
        )
        recipe.save()
        data = {
            'id': recipe.id,
            'name': recipe.name
        }
        return data, HTTPStatus.CREATED


class RecipeResource(Resource):
    # get a specific recipe
    # GET localhost:5000/recipe/1
    def get(self, recipe_id):
        recipe = Recipe()
        rcp = recipe.query.get(recipe_id)

        if rcp is None:
            return {"message": "ID does not exist"}, HTTPStatus.NOT_FOUND

        return {
            'id': rcp.id,
            'name': rcp.name,
            'description': rcp.description,
            'cook_time': rcp.cook_time
        }, HTTPStatus.OK

    # update a recipe
    @jwt_required()
    def put(self, recipe_id):
        recipe = Recipe()
        rcp = recipe.query.get(recipe_id)

        if rcp is None:
            return {"message": "ID does not exist"}, HTTPStatus.NOT_FOUND

        user = User.query.get(get_jwt_identity())

        if not rcp.user_id == user.id:
            # this is not the same user that post the recipe
            return {'message': 'Unauthorized access'}, HTTPStatus.UNAUTHORIZED

        data_json = request.get_json()
        rcp.name = data_json.get('name')
        rcp.description = data_json.get('description')
        rcp.cook_time = data_json.get('cook_time')
        rcp.num_of_servings = data_json.get('num_of_servings')
        rcp.update()

        return {
            'id': recipe_id,
            'name': rcp.name,
            'description': rcp.description,
            'cook_time': rcp.cook_time,
            'num_of_servings': rcp.num_of_servings
        }, HTTPStatus.OK

    # search recipe by name
    def post(self, recipe_id):
        json_data = request.get_json()
        name = json_data.get('name')
        search = f"%{name}%"
        recipes = Recipe.query.filter(Recipe.name.like(search)).all()
        data = []
        for r in recipes:
            data.append({
                'id': r.id,
                'name': r.name,
                'description': r.description,
                'cook_time': r.cook_time
            })
        return data, HTTPStatus.OK


class RecipePublishResource(Resource):
    # update is_publish status from False to True
    # publish
    def put(self, recipe_id):
        recipe = Recipe()
        update_recipe = recipe.query.get(recipe_id)

        if update_recipe is None:
            return {"message": "ID does not exist"}, HTTPStatus.NOT_FOUND

        update_recipe.is_publish = True
        update_recipe.update()
        return {
            "id": recipe_id,
            "name": update_recipe.name
        }, HTTPStatus.OK

    # unpublish. change the is_publish status to False
    def delete(self, recipe_id):
        recipe = Recipe()
        update_recipe = recipe.query.get(recipe_id)

        if update_recipe is None:
            return {"message": "ID does not exist"}, HTTPStatus.NOT_FOUND

        update_recipe.is_publish = False
        update_recipe.update()
        return {
            "id": recipe_id,
            "name": update_recipe.name
        }, HTTPStatus.OK


