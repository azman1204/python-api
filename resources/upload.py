import os
from http import HTTPStatus

from flask_restful import Resource
from flask import request
from werkzeug.utils import secure_filename
from flask import current_app as app


class UploadResource(Resource):
    def post(self):
        if 'doc' not in request.files:
            return {'message': 'No input'}, HTTPStatus.BAD_REQUEST

        doc = request.files['doc']

        if doc and self.allowed_file(doc.filename):
            filename = secure_filename(doc.filename)  # this return secure file name. i.e ../../sample.gif --> sample.gif
            print(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            doc.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return {'message': 'Upload success'}, HTTPStatus.OK
        else:
            return {'message': 'Fail to upload'}, HTTPStatus.BAD_REQUEST

    def allowed_file(self, name):
        allowed_ext = set(['png', 'jpg', 'jpeg', 'gif'])
        # name = sample.gif -> ['sample', 'gif']
        return '.' in name and name.rsplit('.', 1)[1] in allowed_ext
