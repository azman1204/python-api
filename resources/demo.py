from http import HTTPStatus
from flask import request
from flask_restful import Resource
from extensions import cur, validate_token


class DemoResource(Resource):
    def get(self):
        if not validate_token():
            return {'message': 'invalid token'}

        # return all recipes
        sql = "SELECT * FROM recipe"
        cur.execute(sql)
        rows = cur.fetchall()
        data = []
        for recipe in rows:
            data.append({
                'id': recipe['id'],
                'name': recipe['name'],
                'description': recipe['description'],
                'cook_time': recipe['cook_time']
            })

        return data, HTTPStatus.OK

    def post(self):
        if not validate_token():
            return {'message': 'invalid token'}

        json_data = request.get_json()
        sql = f"SELECT * FROM recipe WHERE name LIKE '%{json_data['name']}%'"
        cur.execute(sql)
        rows = cur.fetchall()
        data = []
        for recipe in rows:
            data.append({
                'id': recipe['id'],
                'name': recipe['name'],
                'description': recipe['description'],
                'cook_time': recipe['cook_time']
            })

        return data, HTTPStatus.OK