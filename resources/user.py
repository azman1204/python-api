from flask import request
from flask_restful import Resource
from http import HTTPStatus

from marshmallow import ValidationError

from models.user import User
from util import hash_password
from flask_jwt_extended import jwt_required, get_jwt_identity
from schema.user import UserSchema


user_schema = UserSchema()
user_public_schema = UserSchema(exclude=['email'])


# localhost:5000/users
class UserListResource(Resource):
    def post(self):
        json_data = request.get_json() # return a dictionary

        # validation
        try:
            data = user_schema.load(data=json_data)
        except ValidationError as err:
            return {'message': 'Validation Errors', 'errors': err.messages}, HTTPStatus.BAD_REQUEST

        username = json_data.get('username')
        email = json_data.get('email')
        non_hash_password = json_data.get('password')

        if User.get_by_username(username):
            return {'message': 'username already used!'}, HTTPStatus.BAD_REQUEST

        if User.get_by_email(email):
            return {'message': 'email already used!'}, HTTPStatus.BAD_REQUEST

        password = hash_password(non_hash_password)
        user = User(
            username=username,
            email=email,
            password=password
        )
        # user = data(**data) # this is a user object (deserialization)
        user.save()

        # data = {
        #     'id': user.id,
        #     'username': username,
        #     'email': email
        # }

        # serialization
        data = user_schema.dump(user)  # this is serialization
        return data, HTTPStatus.CREATED

    # list all users
    def get(self):
        users = User.query.all()
        lst = []
        for user in users:
            lst.append({
                'id': user.id,
                'username': user.username
            })

        return lst, HTTPStatus.OK


# localhost:5000/user/<string:username>
class UserResource(Resource):
    # return a user
    # this route / function is protected from unauthorized user
    @jwt_required()
    def get(self, username):
        user = User.get_by_username(username)
        if not user:
            return {'message': 'User not exist'}, HTTPStatus.NOT_FOUND

        return {
            'id': user.id,
            'username': user.username
        }, HTTPStatus.OK

    def put(self, username):
        user = User.get_by_username(username)
        if not user:
            return {'message': 'User not exist'}, HTTPStatus.NOT_FOUND

        json_data = request.get_json()
        user.email = json_data.get('email')
        user.update()
        return {
            'username': user.username
        }, HTTPStatus.OK

    def delete(self, username):
        user = User.get_by_username(username)

        if not user:
            return {'message': 'User does not exist'}, HTTPStatus.NOT_FOUND

        user.remove()
        return {
            "messasge": username + " has been deleted"
        }, HTTPStatus.OK


class MeResource(Resource):
    @jwt_required()
    def get(self):
        print(f'id={get_jwt_identity()}')
        # exit()
        user = User.query.get(get_jwt_identity())

        data = {
            'id': user.id,
            'username': user.username,
            'email': user.email
        }
        return data, HTTPStatus.OK

