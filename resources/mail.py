from flask import render_template
from flask_mail import Mail, Message
from flask_restful import Resource
from flask import current_app as app
from models.recipe import Recipe


class MailResource(Resource):
    def get(self):
        # code to send email here
        mail = Mail()
        msg = Message('Hello Title', sender='admin@gmaial.com', recipients=['azman@yahoo.com'])
        msg.body = 'Hello..'  # plain text, later we do HTML
        mail.send(msg)
        return {'message': 'ok'}, 200


class MailHtmlResource(Resource):
    def get(self):
        # code to send email here
        mail = Mail()
        msg = Message('Hello Title', sender='admin@gmaial.com', recipients=['azman@yahoo.com'])
        msg.html = render_template('email.html')
        mail.send(msg)
        return {'message': 'ok'}, 200


class MailRecipeResource(Resource):
    def get(self):
        recipes = Recipe.query.all()
        # for recipe in recipes:
        #     print(recipe.name)
        msg = Message('Hello Title', sender='admin@gmaial.com', recipients=['azman@yahoo.com'])
        msg.html = render_template('recipe_list.html', recipes=recipes)
        mail = Mail()
        mail.send(msg)
        return {'message': 'ok'}, 200


class MailAttachmentResource(Resource):
    def get(self):
        recipes = Recipe.query.all()
        # for recipe in recipes:
        #     print(recipe.name)
        msg = Message('Hello Title', sender='admin@gmaial.com', recipients=['azman@yahoo.com'])
        msg.text = 'mail with attachment'

        with app.open_resource('C:\\temp\\sample.pdf') as attachment:
            msg.attach('sample.pdf', 'application/pdf', attachment.read())

        mail = Mail()
        mail.send(msg)
        return {'message': 'ok'}, 200