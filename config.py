class Config:
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://john:azman1204@localhost/smilecook'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'azman1204'
    JWT_ERROR_MESSAGE_KEY = 'message'
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    MAIL_SERVER = 'sandbox.smtp.mailtrap.io'
    MAIL_PORT = 2525
    MAIL_USERNAME = '337c8b20e8b609'
    MAIL_PASSWORD = '4e1957a25c985d'
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False

    UPLOAD_FOLDER = 'upload'
